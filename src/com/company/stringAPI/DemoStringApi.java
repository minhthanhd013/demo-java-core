package com.company.stringAPI;

public class DemoStringApi {
    public void demoStringBuilder(){
        StringBuilder str1 = new StringBuilder(10);
        for (int i = 0; i < str1.capacity(); i++) {
            str1.append(i);
            //if(i != str1.capacity()-1)
//
        }
        System.out.println(str1.toString());
        System.out.println("Reverse string: ");
        str1.reverse();
        System.out.println(str1.toString());
        System.out.println("Index of '1': "+str1.indexOf("1"));

        StringBuilder str2 = new StringBuilder(3);
        str2.append(2);
        str2.append(2);
        str2.append(1);
        System.out.println("Latest index of '2': "+str2.lastIndexOf("2"));
    }
    public void demoStringBuffer(){
        StringBuffer str1 = new StringBuffer("Thanh");
        str1.append("Do");
        System.out.println(str1.toString());
        System.out.println("Insert ' ' to string: ");
        str1.insert(5," ");
        System.out.println(str1.toString());
        System.out.println("Replace ' ' with '+': ");
        str1.replace(5,6,"+");
        System.out.println(str1.toString());
        System.out.println("Reverse string: ");
        str1.reverse();
        System.out.println(str1.toString());
    }
}
