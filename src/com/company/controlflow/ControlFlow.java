package com.company.controlflow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ControlFlow {
    public void for_loop(){
        System.out.println("For loop to print out the number from 0 to 9:");
        for (int i = 0; i < 10; i++) {
            System.out.println("\t"+i);
        }
    }
    public void while_loop(){
        // stop the loop when stop_con = 10;
        System.out.println("While loop to print out the number from 0 to 9");
        int stop_con = 0;
        while (stop_con<10){
            System.out.println("\t"+stop_con);
            stop_con++;
        }
    }
    public void do_while_loop(){
        // find sum from 1-Nth
        System.out.println("This function use do_while loop to calculate the sum of integers from 1 to N backward!!");
        Scanner sc = new Scanner(System.in);
        System.out.println("\tInput N value: ");
        int n = sc.nextInt();
        int temp = n;
        int sum = 0;
        do{
            sum+= temp;
            temp--;
        }while (temp > 0);
        System.out.println("\tSum of integers from 1 to "+n+": "+sum);
    }
    public void foreach_loop(){
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(null);
        list1.add(1);

        list1.add(2);
        list1.add(3);
        System.out.println("Foreach loop find min value from the list:");
        Integer min = 0;
        if(list1.get(0)!=null){
            min = list1.get(0);
        }
        else min = list1.get(1);

        for(Integer res: list1){
            if(res!= null && res < min)
                min = res;
        }
        System.out.println("\tMin value of list is: "+min);
    }

    // condition statements

    public void if_condition(int temp){
        System.out.println("If condition to check if integer is odd/even:");
        if(temp%2 == 0){
            System.out.println("This integer "+temp+" is even!");
        }else System.out.println("This integer "+temp+" is odd!");
    }
    public void switch_condition(){
        System.out.println("Switch condition to check today fortune!");
        // get random int
        int random_int = (int)Math.floor(Math.random()*(10-1+1)+1);
        int res = random_int % 2 == 0 ? 1 : 0;
        switch (res){
            case 1:{
                System.out.println("\tToday is your lucky day!");
                break;
            }
            case 0:{
                System.out.println("\tToday is your bad day!");
                break;
            }
        }
    }
    public void sort_Array() {
        int len = 10;
        int a[] = new int[len];
        for (int i = 0; i < len; i++) {
            a[i] = i;
        }
        int temp;
        for (int i = 0; i < len - 1; i++) {
            for (int j = i + 1; j < len; j++) {
                if (a[j] < a[i]) {
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
        System.out.println("Sort ASC: ");
        for (int i = 0; i < len; i++) {
            System.out.print(a[i]+"\t");
        }
        System.out.println("\n");
    }
    public void multidimensionalArray(){
        int row = 3;
        int col = 3;
        int matrix[][] = new int[3][3];
        int val = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                matrix[i][j] = val;
                val++;
            }
        }
        System.out.println("Matrix with 3 rows and 3 columns");
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.print(matrix[i][j]+"\t");
            }
            System.out.println();
        }
    }
}
