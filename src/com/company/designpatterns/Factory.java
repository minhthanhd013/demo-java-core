package com.company.designpatterns;

public class Factory {
    public static void main(String[] args) {
        FactoryCoach theFactory = new FactoryCoach();
        System.out.println(theFactory.getSalary("Baseball Coach", 12));
    }
}

/**
 * This class will use factory design pattern to get salary from each coach, instead of creating duplicate methods in each Coach class.
 */
class FactoryCoach{
    public double getSalary(String typeOfCoach, int workHours){
        if(typeOfCoach.equals("Baseball Coach")){
            return workHours * 200;
        } else if(typeOfCoach.equals("Football Coach")){
            return workHours * 150;
        } else if(typeOfCoach.equals("Volleyball Coach")){
            return workHours * 100;
        } else{ return 0;}
    }
}
