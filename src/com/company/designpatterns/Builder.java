package com.company.designpatterns;


public class Builder {
    public static void main(String[] args) {
    Coach newCoach = new Coach.CoachBuilder("Thanh").setAge(21).setJob("Football Coach").build();
        System.out.println(newCoach);
    }

}
/**
 * This class is use to demo builder design pattern.
 * Use case: prevent user from entering so many values in constructors.
 */
class Coach{
    private String name;
    private int age;
    private String job;

    @Override
    public String toString() {
        return "Coach{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", job='" + job + '\'' +
                '}';
    }

    public Coach(CoachBuilder coachBuilder) {
        this.name = coachBuilder.name;
        this.age = coachBuilder.age;
        this.job = coachBuilder.job;
    }
    public static class CoachBuilder{
        private String name;
        private int age;
        private String job;
        public CoachBuilder(String name) {
            this.name = name;
        }
        public CoachBuilder setAge(int age){
            this.age = age;
            return this;
        }
        public CoachBuilder setJob(String job){
            this.job = job;
            return this;
        }
        public Coach build(){
            return new Coach(this);
        }
    }
}

