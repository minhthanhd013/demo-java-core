package com.company.designpatterns;


public class Singleton {
    public static void main(String[] args) {
        Singleton_Eager sing1 = Singleton_Eager.getInstance();
        Singleton_Eager sing2 = Singleton_Eager.getInstance();
        System.out.println("Address to integer: "+sing1.hashCode());
        System.out.println("Address to integer: "+sing2.hashCode());
        System.out.println("Check if 2 var sing1 and sing2 are equal: "+sing1.equals(sing2));
        Singleton_Lazy sing3 = Singleton_Lazy.getInstance();
        Singleton_Lazy sing4 = Singleton_Lazy.getInstance();
        System.out.println("Address to integer: "+sing3.hashCode());
        System.out.println("Address to integer: "+sing4.hashCode());
        System.out.println("Check if 2 var sing3 and sing4 are equal: "+sing3.equals(sing4));
    }

}

class Singleton_Eager{
    public static   Singleton_Eager temp = new Singleton_Eager();
    private Singleton_Eager(){
        System.out.println("New instance created");
    }

    /**
     * This instance is created at line 20. So it is the same.
     * @return
     */
    public static Singleton_Eager getInstance(){
        return temp;
    }
}
class Singleton_Lazy{
    public static Singleton_Lazy temp;
    private Singleton_Lazy(){
        System.out.println("New instance created");
    }

    /**
     * This function will create a new Instance when it being called
     * @return
     */
    public static Singleton_Lazy getInstance(){
        temp = new Singleton_Lazy();

        return temp;
    }
}
