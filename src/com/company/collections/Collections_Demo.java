package com.company.collections;

import java.util.*;

public class Collections_Demo {
    /**
     * Demo iterator with Arraylist and Iterator
     */
    public void iterator(){
        List<Integer> temp = new ArrayList<>();
        temp.add(1);
        temp.add(2);
        temp.add(3);

        Iterator<Integer> temp_iterator = temp.iterator();
        while(temp_iterator.hasNext()){
            System.out.print(temp_iterator.next()+"\t");
        }
        System.out.println("\n");
    }
    /**
     * Demo iterator with Arraylist and ListIterator
     */
    public void list_iterator(){
        List<Integer> temp = new ArrayList<>();
        temp.add(1);
        temp.add(2);
        temp.add(3);
        temp.add(4);

        ListIterator<Integer> tempListiterator = temp.listIterator();
        System.out.println("Forward traverse:");
        while(tempListiterator.hasNext()){
            System.out.print(tempListiterator.next()+"\t");
        }
        System.out.println("\n");
        System.out.println("Backward traverse:");
        while(tempListiterator.hasPrevious()){
            System.out.print(tempListiterator.previous()+"\t");
        }
        System.out.println("\n");
    }
    public void linkedList(){
        List<Integer> newList = new LinkedList<>();
        newList.add(1);
        newList.add(2);
        newList.add(3);
        newList.add(4);
        newList.add(5);
        newList.add(6);
        newList.add(7);
        System.out.println("List before add last/first");
        System.out.println(newList);
        System.out.println("Add first with value 10");
        newList.add(0,10);
        System.out.println(newList);
        System.out.println("Add last with value 100");
        newList.add(newList.size(), 100);
        System.out.println(newList);
        System.out.println("Remove first:");
        newList.remove(0);
        System.out.println(newList);
        System.out.println("Remove last:");
        newList.remove(newList.size()-1);
        System.out.println(newList);
    }
    public void setAndSortedSet(){
        Set<Integer> newSet = new HashSet<>();
        Integer a[] ={1,2,3,4,5,6,8,7};
        Collections.addAll(newSet, a);
        System.out.println("Set: ");
        System.out.println(newSet);
        System.out.println("Check if set contains value 1: "+newSet.contains(1));
        System.out.println("Sort ASC set: ");
        Set<Integer> sortedSet = new TreeSet<>(newSet);
        System.out.println(sortedSet);
        System.out.println("Remove 1 value from set: ");
        newSet.remove(1);
        System.out.println(newSet);
        System.out.println("Clear the entire set: ");
        newSet.clear();
        System.out.println(newSet);

    }
    public void demoHashMap(){
        Map<Integer, String> newMap = new HashMap<>();
        newMap.put(1,"a");
        newMap.put(2,"b");
        System.out.println("Check if HashMap contains key 1, and value b: "+newMap.containsKey(1)+", "+newMap.containsValue("b"));
        newMap.putIfAbsent(3,"c");
        System.out.println(newMap);
        System.out.println("Get set of key from hashmap: ");
        Collection<Integer> keyInt = newMap.keySet();
        System.out.println(keyInt);
        System.out.println("Get set of values from hashmap: ");
        Collection<String> valueString = newMap.values();
        System.out.println(valueString);
        newMap.forEach((key,value)->{
            System.out.println("Key: "+key);
            System.out.println("Value: "+value);
        });
        // add value new key and value to hashmap if key not found in the Hashmap
        newMap.computeIfAbsent(4, value -> {return "d";});
        System.out.println(newMap);
        // change value if key is even(change to 2+value) and 1+value if odd
        newMap.computeIfPresent(4,(key,value)->{
            if(key%2==0) value = 2+value;
            else value = 1+value;
            return value;
        });
        System.out.println(newMap);

    }
    public void demoQueue(){

        Queue<Integer> newQueue = new LinkedList<>();
        newQueue.add(1);
        newQueue.add(2);
        newQueue.add(3);
        newQueue.add(4);
        System.out.println(newQueue);
        newQueue.offer(5);
        System.out.println(newQueue);
        System.out.println("Delete from head: ");
        newQueue.remove();

        System.out.println("Delete each element from list(null if out of range) with current size = "+newQueue.size()+":");
        System.out.println(newQueue);
        System.out.println(newQueue.poll());
        System.out.println(newQueue.poll());
        System.out.println(newQueue.poll());
        System.out.println(newQueue.poll());
        System.out.println(newQueue.poll());


        //System.out.println(newQueue2.peek());


    }
}
