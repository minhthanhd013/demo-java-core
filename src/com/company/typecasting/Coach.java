package com.company.typecasting;

public class Coach {
    private String name;

    public Coach(String name) {
        this.name = name;
    }

    public void getJob(){

    }

    public String getName() {
        return name;
    }
}
