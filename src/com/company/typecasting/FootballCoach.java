package com.company.typecasting;

public class FootballCoach extends Coach{


    public FootballCoach(String name) {
        super(name);
    }

    public void getJob(){
        System.out.println("\tCoach name: "+super.getName());
        System.out.println("\tRun 2 times around the pitch!");
    }
}
