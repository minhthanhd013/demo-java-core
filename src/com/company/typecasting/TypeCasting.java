package com.company.typecasting;

public class TypeCasting {
//    Widen casting
    public void WideningCasting(int a){
        double res = a;
        System.out.println("Widening casting:");
        System.out.println("\tint: "+a);
        System.out.println("\tdouble: "+res);
    }
    public void NarrowingCasting(double a){
        int res = (int) a;
        System.out.println("Narrowing casting:");
        System.out.println("\tdouble: "+a);
        System.out.println("\tint: "+res);
    }
    public void AutoAndUnBoxing(){
        // Unboxing:
        Integer temp = new Integer(100);
        int a = temp;
        System.out.println("Unboxing:");
        System.out.println("\t"+temp);
        System.out.println("\t"+a);

        // Autoboxing
        Character res = 'a';
        char temp2 = res;
        System.out.println("Autoboxing:");
        System.out.println("\t"+res);
        System.out.println("\t"+temp2);

    }
    public void UpcastingAndDowncasting(){
        // Up casting:
        Coach theCoach = new FootballCoach("Thanh");
        System.out.println("Upcasting");
        theCoach.getJob();
        // Down casting:
        System.out.println("Downcasting");
        FootballCoach theFootballCoach  = (FootballCoach) theCoach;
        theFootballCoach.getJob();
    }
}
