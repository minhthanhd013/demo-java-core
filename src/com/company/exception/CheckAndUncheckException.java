package com.company.exception;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CheckAndUncheckException {
    /**
     * Checked exception is thrown at compile time
     */

    public void checkException() throws IOException {

        FileInputStream fileInputStream = null;
        fileInputStream = new FileInputStream("D:/notExistFile.txt");
        int i;
        while(( i = fileInputStream.read() ) != -1)
        {
            System.out.print((char)i);
        }
        fileInputStream.close();
    }

    /**
     * This function will compile ok but will throw exception at runtime.
     */
    public void unCheckException() {
        int a[] ={1,2,3,4};
        System.out.println(a[4]);

    }
}
