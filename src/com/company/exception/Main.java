package com.company.exception;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        CheckAndUncheckException check = new CheckAndUncheckException();
        check.checkException();
        check.unCheckException();
    }
}
