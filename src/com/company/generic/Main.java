package com.company.generic;

public class Main {
    public static void main(String[] args) {
        // demo generic:
        GenericDemo<Integer> gene1 = new GenericDemo<>(10);
        GenericDemo<Double> gene2 = new GenericDemo<>(10.0);
        gene1.getType();
        gene2.getType();
        gene1.tichHaiSo(gene2);

        HinhVuong<Integer> gene3 = new HinhVuong<>(100);
        gene3.tinhDienTich();
    }
}
