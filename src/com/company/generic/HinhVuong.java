package com.company.generic;

public class HinhVuong<A extends Number> {
    private A temp;

    public HinhVuong(A temp) {
        this.temp = temp;

    }

    public void tinhDienTich(){
        System.out.println("Dien tich hinh vuong:"+ (temp.intValue() * temp.intValue()));
    }
}
