package com.company.generic;

public class GenericDemo <A extends Number>{
    private A temp1;

    public GenericDemo(A temp1) {
        this.temp1 = temp1;
    }

    public A getTemp1() {
        return temp1;
    }

    public void getType(){
        System.out.println(temp1.getClass().getName());
    }
    public void tichHaiSo(GenericDemo<?> temp2){
        System.out.println("Tich hai so: "+(temp1.doubleValue() * temp2.getTemp1().doubleValue()));
    }
}
