package com.company;

import com.company.collections.Collections_Demo;
import com.company.controlflow.ControlFlow;
import com.company.generic.HinhVuong;
import com.company.generic.GenericDemo;
import com.company.typecasting.TypeCasting;

public class Main {

    public static void main(String[] args) {
        // Demo type casting:
        TypeCasting type1 = new TypeCasting();
        type1.WideningCasting(10);
        type1.NarrowingCasting(100);
        type1.AutoAndUnBoxing();
        type1.UpcastingAndDowncasting();

        // Demo Control flow:
        ControlFlow control1 = new ControlFlow();
        control1.do_while_loop();
        control1.foreach_loop();
        control1.switch_condition();
        control1.sort_Array();
        control1.multidimensionalArray();

        // Demo collections

        Collections_Demo col1 = new Collections_Demo();
        col1.iterator();
        col1.list_iterator();
        col1.linkedList();
        col1.setAndSortedSet();
        col1.demoHashMap();
        col1.demoQueue();


    }
}
