package com.company.inputoutputfile;

import java.io.*;

/**
 * This class using BufferInputStream/BufferOutputStream to read/write file faster than FileInputStream/OutputStream
 */
public class WriterReader3 {
    public static void main(String[] args) throws IOException {
        File f1 = new File("abc2.txt");
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(f1));
        String s = "Do Nguyen Minh Thanh";
        byte temp[] = s.getBytes();
        bufferedOutputStream.write(temp);
        bufferedOutputStream.close();

        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(f1));

        int i = 0;
        while ((i = bufferedInputStream.read()) != -1) {
            // retrieve the value of ASCII: -> char
            System.out.print((char)i);
        }
        bufferedInputStream.close();
    }

}
