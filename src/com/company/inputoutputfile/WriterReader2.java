package com.company.inputoutputfile;

import java.io.*;

/**
 * This class is use InputStream and OutputStream to read/write to file.
 * Write: convert String to byte-> write
 */
public class WriterReader2 {
    public static void main(String[] args) throws IOException {

            OutputStream fOut
                    = new FileOutputStream("abc.txt");
            String s = "abc";

            // converting string into byte array
            byte temp[] = s.getBytes();
            fOut.write(temp);
            fOut.close();
            InputStream fIn
                    = new FileInputStream("abc.txt");
            int i = 0;
            while ((i = fIn.read()) != -1) {
                // retrieve the value of ASCII: -> char
                System.out.print((char)i);
            }
            fIn.close();


        
    }
}
