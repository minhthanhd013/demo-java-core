package com.company.inputoutputfile;

import java.io.Serializable;

public class Coach implements Serializable {

    private static final long serialVersionUID = 1L;
    private String name;
    private String position;
    private double salary;

    Coach() {
    };

    Coach(String name,String position, double salary) {
        this.name = name;
        this.position = position;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Name:" + name + "\nPosition: " + position + "\nSalary: " + salary;
    }
}