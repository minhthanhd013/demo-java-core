package com.company.inputoutputfile;

import java.io.*;

/**
 * Write(Using Serialization) and Read file using FileOutputStream and FileInputStream
 */
public class WriterReader {

    public static void main(String[] args) {

        Coach theCoach1 = new Coach("Thanh", "Football Coach", 100000);
        Coach theCoach2 = new Coach("Thanh1", "Baseball Coach", 120000);

        try {
            File tmpFile = new File("theCoach.txt");
            FileOutputStream f = new FileOutputStream(tmpFile);
            ObjectOutputStream o = new ObjectOutputStream(f);

            // Write objects to file
            o.writeObject(theCoach1);
            o.writeObject(theCoach2);

            o.close();
            f.close();

            FileInputStream fi = new FileInputStream(tmpFile);
            ObjectInputStream oi = new ObjectInputStream(fi);

            // Read objects
            Coach c1 = (Coach) oi.readObject();
            Coach c2 = (Coach) oi.readObject();

            System.out.println(c1.toString());
            System.out.println(c2.toString());

            oi.close();
            fi.close();

        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

}