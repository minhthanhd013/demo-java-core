package com.company.java8;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Java8DemoStream {
    public static void main(String[] args) {
        IntStream
                .range(1,5)
                .skip(2)
                .forEach(x-> System.out.println(x));
        System.out.println("Sum: "+
                IntStream
                        .range(1,5)
                        .skip(2)
                        .sum());
        Stream.of("Ava","Aneri", "Alberto")
                .sorted()
                .forEach(x-> System.out.println(x));

        Stream.of("Ava","Aneri", "Alberto")
                .sorted()
                .findFirst()
                .ifPresent(System.out::println);

        String[] names = {"a","b", "c"};
        Arrays.stream(names)
                .filter(x->x.startsWith("a"))
                .sorted()
                .forEach(System.out::println);
        Arrays.stream(new int[]{1,2,3,4,5,6})
                .map(x->x*x)
                .average()
                .ifPresent(System.out::println);
        List<String> pepple =Arrays.asList("AL","Bss");
        pepple
                .stream()
                .map(String::toLowerCase)
                .filter(x-> x.startsWith("a"))
                .forEach(System.out::println);
        IntSummaryStatistics summaryStatistics = IntStream.of(1,2,3,4,5,6)
                .summaryStatistics();
        System.out.println(summaryStatistics);
    }
}
