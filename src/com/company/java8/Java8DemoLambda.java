package com.company.java8;

import java.util.HashMap;
import java.util.Map;

public class Java8DemoLambda {

    public static void main(String[] args) {
        // Demo lambda expression on collections:
        Map<Integer, String> temp = new HashMap<>();
        temp.put(1, "ABC");
        temp.put(2, "DEF");
        System.out.println(temp);
        temp.computeIfPresent(2,(k,v)->{
            if(k%2 == 0) v+="2";
            else  v+="1";
           return v;
        });
        System.out.println(temp);
        // Demo lambda expression on thread

        Thread thread1 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                System.out.println("Thread 1>>> "+ i);

                try {
                    Thread.sleep(100);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        } );
        Thread thread2 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                System.out.println("Thread 2>>> "+ i);
                try {
                    Thread.sleep(100);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        } );
        thread1.start();
        thread2.start();

        //
    }
}
